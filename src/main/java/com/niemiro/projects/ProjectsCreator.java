package com.niemiro.projects;

import com.niemiro.projects.dto.ProjectDto;
import lombok.AllArgsConstructor;

@AllArgsConstructor
class ProjectsCreator
{
    private ProjectsRepository projectsRepository;

    public void createProject(ProjectDto projectDto)
    {
        projectsRepository.createProject(projectDto);
    }
}
