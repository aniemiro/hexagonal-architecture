package com.niemiro.projects;

import com.niemiro.projects.dto.ProjectDto;

import java.util.List;

public interface ProjectsRepository
{
    void createProject(ProjectDto projectDto);
    List<ProjectDto> getProjects();
}
