package com.niemiro.projects;

import com.niemiro.projects.dto.ProjectDto;
import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class ProjectsFacade
{
    private ProjectsFinder projectsFinder;
    private ProjectsCreator projectsCreator;

    public void createProject(ProjectDto projectDto)
    {
        projectsCreator.createProject(projectDto);
    }

    public List<ProjectDto> getProjects()
    {
        return projectsFinder.getProjects();
    }
}
