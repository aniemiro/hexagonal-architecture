package com.niemiro.projects;

import com.niemiro.projects.dto.ProjectDto;
import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
class ProjectsFinder
{
    private ProjectsRepository projectsRepository;

    public List<ProjectDto> getProjects()
    {
        return projectsRepository.getProjects();
    }
}
