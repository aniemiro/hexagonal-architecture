package com.niemiro.projects.infrastructure.repository;

import com.niemiro.projects.ProjectsRepository;
import com.niemiro.projects.dto.ProjectDto;
import lombok.AllArgsConstructor;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@AllArgsConstructor
class SpringDataProjectsRepository implements ProjectsRepository
{
    private CrudProjectsRepository projectsRepository;

    @Override
    @Transactional
    public void createProject(ProjectDto projectDto)
    {
        if (projectDto != null)
        {
            Project project = new Project(projectDto.getId(), projectDto.getName());
            projectsRepository.save(project);
        }
    }

    @Override
    public List<ProjectDto> getProjects()
    {
        return projectsRepository.getProjects();
    }
}
