package com.niemiro.projects.infrastructure.repository;

import com.niemiro.projects.ProjectsRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class ProjectsRepositoryConfiguration
{
    @Bean
    ProjectsRepository projectsRepository(CrudProjectsRepository crudProjectsRepository)
    {
        return new SpringDataProjectsRepository(crudProjectsRepository);
    }
}
