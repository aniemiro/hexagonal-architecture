package com.niemiro.projects.infrastructure.repository;

import com.niemiro.projects.dto.ProjectDto;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
interface CrudProjectsRepository extends CrudRepository<Project, Long>
{
    @Query("select new com.niemiro.projects.dto.ProjectDto(id, name) from project")
    List<ProjectDto> getProjects();
}
