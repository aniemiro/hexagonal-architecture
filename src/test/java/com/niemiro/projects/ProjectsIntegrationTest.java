package com.niemiro.projects;

import com.niemiro.projects.dto.ProjectDto;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
class ProjectsIntegrationTest extends IntegrationTest
{
    @Autowired
    ProjectsFacade projectsFacade;

    @Test
    public void test()
    {
        ProjectDto projectDto = new ProjectDto(1L, "Test project 1");
        projectsFacade.createProject(projectDto);
        log.info("Project created.");

        projectsFacade.getProjects()
            .forEach(project -> log.info(project.toString()));
    }
}
