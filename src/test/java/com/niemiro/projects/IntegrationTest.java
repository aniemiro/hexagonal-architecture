package com.niemiro.projects;

import com.niemiro.ProjectsApplication;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Testcontainers;

@ActiveProfiles("test")
@SpringBootTest
@ContextConfiguration(classes = { ProjectsApplication.class})
@Testcontainers
@EnableTransactionManagement
class IntegrationTest
{
    public static PostgreSQLContainer<?> container = new PostgreSQLContainer<>("postgres:12.5")
        .withUsername("1")
        .withPassword("1")
        .withDatabaseName("localhost_db");

    @BeforeAll
    static void beforeAll()
    {
        container.start();
    }

    @DynamicPropertySource
    static void configureProperties(DynamicPropertyRegistry registry)
    {
        registry.add("spring.datasource.url", container::getJdbcUrl);
        registry.add("spring.datasource.username", container::getUsername);
        registry.add("spring.datasource.password", container::getPassword);
        registry.add("spring.jpa.hibernate.ddl-auto", () -> "create-drop");
    }
}
