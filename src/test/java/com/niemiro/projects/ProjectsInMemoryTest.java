package com.niemiro.projects;

import com.niemiro.projects.dto.ProjectDto;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@Slf4j
class ProjectsInMemoryTest extends InMemoryTest
{
    ProjectsFacade projectsFacade = ProjectsFacadeFactory.projectsFacade(new InMemoryProjectsRepository());

    @Test
    public void test()
    {
        ProjectDto projectDto = new ProjectDto(1L, "Test project 1");
        projectsFacade.createProject(projectDto);
        log.info("Project created in InMemory database.");

        projectsFacade.getProjects()
            .forEach(project -> log.info(project.toString()));
    }
}
