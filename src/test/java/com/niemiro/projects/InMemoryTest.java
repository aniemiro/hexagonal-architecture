package com.niemiro.projects;

import com.niemiro.projects.dto.ProjectDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.test.context.ActiveProfiles;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ActiveProfiles("test")
@Slf4j
class InMemoryTest
{
    static class InMemoryProjectsRepository implements ProjectsRepository
    {
        private Map<Long, ProjectDto> db = new HashMap<>();

        @Override
        public void createProject(ProjectDto projectDto)
        {
            if (projectDto != null)
            {
                db.put(projectDto.getId(), projectDto);
            }
        }

        @Override
        public List<ProjectDto> getProjects()
        {
            return db.values()
                .stream()
                .toList();
        }
    }
}
